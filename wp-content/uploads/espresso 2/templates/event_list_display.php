<?php
//This is the event list template page.
//This is a template file for displaying an event lsit on a page.
//There should be a copy of this file in your wp-content/uploads/espresso/ folder.
/*
 * use the following shortcodes in a page or post:
 * [EVENT_LIST]
 * [EVENT_LIST limit=1]
 * [EVENT_LIST css_class=my-custom-class]
 * [EVENT_LIST show_expired=true]
 * [EVENT_LIST show_deleted=true]
 * [EVENT_LIST show_secondary=false]
 * [EVENT_LIST show_recurrence=true]
 * [EVENT_LIST category_identifier=your_category_identifier]
 *
 * Example:
 * [EVENT_LIST limit=5 show_recurrence=true category_identifier=your_category_identifier]
 *
 */

//Print out the array of event status options
//print_r (event_espresso_get_is_active($event_id));
//Here we can create messages based on the event status. These variables can be echoed anywhere on the page to display your status message.
$status = event_espresso_get_is_active(0,$event_meta);
$status_display = ' - ' . $status['display_custom'];
$status_display_ongoing = $status['status'] == 'ONGOING' ? ' - ' . $status['display_custom'] : '';
$status_display_deleted = $status['status'] == 'DELETED' ? ' - ' . $status['display_custom'] : '';
$status_display_secondary = $status['status'] == 'SECONDARY' ? ' - ' . $status['display_custom'] : ''; //Waitlist event
$status_display_draft = $status['status'] == 'DRAFT' ? ' - ' . $status['display_custom'] : '';
$status_display_pending = $status['status'] == 'PENDING' ? ' - ' . $status['display_custom'] : '';
$status_display_denied = $status['status'] == 'DENIED' ? ' - ' . $status['display_custom'] : '';
$status_display_expired = $status['status'] == 'EXPIRED' ? ' - ' . $status['display_custom'] : '';
$status_display_reg_closed = $status['status'] == 'REGISTRATION_CLOSED' ? ' - ' . $status['display_custom'] : '';
$status_display_not_open = $status['status'] == 'REGISTRATION_NOT_OPEN' ? ' - ' . $status['display_custom'] : '';
$status_display_open = $status['status'] == 'REGISTRATION_OPEN' ? ' - ' . $status['display_custom'] : '';

//You can also display a custom message. For example, this is a custom registration not open message:
$status_display_custom_closed = $status['status'] == 'REGISTRATION_CLOSED' ? ' - <span class="espresso_closed">' . __('Regsitration is closed', 'event_espresso') . '</span>' : '';
global $this_event_id;
$this_event_id = $event_id;

$date = explode(' ', $start_date);
$date = explode('-', $date[0]);
$month = $date[1];
switch ($month) {
	case '1':
		$month = 'Jan';
		break;
	case '2':
		$month = 'Feb';
		break;
	case '3':
		$month = 'Mar';
		break;
	case '4':
		$month = 'Apr';
		break;
	case '5':
		$month = 'May';
	case '6':
		$month = 'Jun';
		break;
	case '7':
		$month = 'Jul';
		$break;
	case '8':
		$month = 'Aug';
		break;
	case '9':
		$month = 'Sep';
		break;
	case '10':
		$month = 'Oct';
		break;
	case '11':
		$month = 'Nov';
		break;
	case '12':
		$month = 'Dec';
		break;

}
$date = $month . ' ' . $date[2] . ', ' . $date[0];
$spaces = get_number_of_attendees_reg_limit($event_id, 'available_spaces');
$location = $venue_address . ' ' . $venue_city . ' ' . $venue_state . ' ' . $venue_zip . ' ' . $venue_country;
$time = $event_meta['start_time'];

$time = explode(':', $time);
$min = $time[1];
$time = intval($time[0]);

if ($time < 12) {
	$time = strval($time) . ':'. $min .'am';
}else if ($time >= 12){
	$time = $time - 12;
	$time = strval($time) . ':'. $min .'pm';
}


?>
<tr>
    <td><?php echo $date ?></td>
    <td><?php echo $event_id ?></td>
    <td><a href="http://maps.google.com/?q=<?php echo $location; ?>" target="_blank"><?php echo $venue_title ?></a></td>
    <td><?php echo $time ?></td>
    <td><?php echo '$'.$event->event_cost = empty($event->event_cost) ? '' : $event->event_cost; ?></td>
    <td>
    	<?php if ($spaces == '0'): ?>
    		<span>Closed</span>
    	<?php else : ?>
    		<a class="btn book-now" href="<?php echo $registration_url; ?>">Book</a>
    	<?php endif; ?>
    </td>
  </tr>

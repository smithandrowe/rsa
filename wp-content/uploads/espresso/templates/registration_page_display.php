<?php
//This is the registration form.
//This is a template file for displaying a registration form for an event on a page.
//There should be a copy of this file in your wp-content/uploads/espresso/ folder.

$date = explode(' ', $start_date);
$date = explode('-', $date[0]);
$month = $date[1];
switch ($month) {
	case '1':
		$month = 'Jan';
		break;
	case '2':
		$month = 'Feb';
		break;
	case '3':
		$month = 'Mar';
		break;
	case '4':
		$month = 'Apr';
		break;
	case '5':
		$month = 'May';
	case '6':
		$month = 'Jun';
		break;
	case '7':
		$month = 'Jul';
		$break;
	case '8':
		$month = 'Aug';
		break;
	case '9':
		$month = 'Sep';
		break;
	case '10':
		$month = 'Oct';
		break;
	case '11':
		$month = 'Nov';
		break;
	case '12':
		$month = 'Dec';
		break;

}
$date = $month . ' ' . $date[2] . ', ' . $date[0];
$starttime = $all_meta['start_time'];

$starttime = explode(':', $starttime);
$startmin = $starttime[1];
$starttime = intval($starttime[0]);

if ($starttime < 12) {
	$starttime = strval($starttime) . ':'. $startmin .'am';
}else if ($starttime >= 12){
	if ($starttime != 12) {
		$starttime = $starttime - 12;	
	}
	$starttime = strval($starttime) . ':'. $startmin .'pm';
}

$endtime = $all_meta['end_time'];

$endtime = explode(':', $endtime);
$endmin = $endtime[1];
$endtime = intval($endtime[0]);

if ($endtime < 12) {
	$endtime = strval($endtime) . ':'. $endmin .'am';
}else if ($endtime >= 12){
	if ($endtime != 12) {
		$endtime = $endtime - 12;	
	}
	
	$endtime = strval($endtime) . ':'. $endmin .'pm';
}



?>


<div id="registration" class="row">
	<div class="col-xs-6">
		<h1 class="title">Step 1 - Registration</h1>
		<h3>Submit your details</h3>
		<p>After submitting your details, you will be asked for payment. Please note that your registration will not be accepted until after payment has been made.</p>
		<h3>Course information</h3>
		<p><?php echo $event_name ?></p>
		<p><?php echo espresso_format_content($event_desc); //Code to show the actual description. The Wordpress function "wpautop" adds formatting to your description.   ?></p>
		<h3>Venue address:</h3>
		<span><?php echo $venue_title ?></span>
		<p><?php echo $venue_address ?>, <?php echo $venue_city ?> <?php echo $venue_state ?> <?php echo $venue_zip ?></p>
		<div id="event-date" class="row">
			<div class="col-xs-3"><span>Date:</span></div>
			<div class="col-xs-9"><span><?php echo $date; ?></span> <span data-toggle="tooltip" data-placement="right"><?php echo apply_filters('filter_hook_espresso_display_ical', $all_meta); ?></span></div>
			<div class="col-xs-3"><span>Start Time:</span></div>
			<div class="col-xs-9"><span><?php echo $starttime; ?></span></div>
			<div class="col-xs-3"><span>End Time:</span></div>
			<div class="col-xs-9"><span><?php echo $endtime; ?></span></div>
			<div class="tooltip">
				<div class="tooltip-inner">Tooltip!</div><div class="tooltip-arrow"></div></div>
			</div>
		<?php 	switch ($is_active['status']) {
	
				case 'EXPIRED': 
					//only show the event description.
					echo '<h3 class="expired_event">' . __('This event has passed.', 'event_espresso') . '</h3>';
				break;

				case 'REGISTRATION_CLOSED': 
		
				//only show the event description.
				// if todays date is after $reg_end_date
		?>
		<div class="event-registration-closed event-messages ui-corner-all ui-state-highlight">
			<span class="ui-icon ui-icon-alert"></span>
			<p class="event_full">
				<strong>
					<?php _e('We are sorry but registration for this event is now closed.', 'event_espresso'); ?>
				</strong>
			</p>
			<p class="event_full">
				<strong>
					<?php  _e('Please ', 'event_espresso');?><a href="contact" title="<?php  _e('contact us ', 'event_espresso');?>"><?php  _e('contact us ', 'event_espresso');?></a><?php  _e('if you would like to know if spaces are still available.', 'event_espresso'); ?>
				</strong>
			</p>
		</div>
		<?php
			break;

			case 'REGISTRATION_NOT_OPEN': 
			//only show the event description.
			// if todays date is after $reg_end_date
			// if todays date is prior to $reg_start_date
		?>
			<div class="event-registration-pending event-messages ui-corner-all ui-state-highlight">
				<span class="ui-icon ui-icon-alert"></span>
					<p class="event_full">
						<strong>
							<?php _e('We are sorry but this event is not yet open for registration.', 'event_espresso'); ?>
						</strong>
					</p>
					<p class="event_full">
						<strong>
							<?php echo  __('You will be able to register starting ', 'event_espresso') . ' ' . event_espresso_no_format_date($reg_start_date, 'F d, Y'); ?>
						</strong>
					</p>
				</div>
		<?php
				break;
		
				default: //This will display the registration form
				
				do_action('action_hook_espresso_registration_page_top', $event_id, $event_meta, $all_meta);
		?>
			<div class="event_espresso_form_wrapper">
				<form method="post" action="<?php echo get_permalink( $event_page_id );?>" id="registration_form">
					<?php
							//This hides the date/times and location when usign custom post types or the ESPRESSO_REG_FORM shortcode
						if ( $reg_form_only == false ){	
							/* Display the address and google map link if available */
							if ($location != '' && (empty($org_options['display_address_in_regform']) || $org_options['display_address_in_regform'] != 'N')) {
					?>
						
					<?php	}
							do_action('action_hook_espresso_social_display_buttons', $event_id);
					?>
		
					
					<?php	}
							// * * This section shows the registration form if it is an active event * *
				
							if ($display_reg_form == 'Y') {
								
								do_action('action_hook_espresso_registration_form_top', $event_id, $event_meta, $all_meta);
					?>
						
		
		
						<div id="event-reg-form-groups">
						
							<h3><?php _e('Quick and Easy Registration!', 'event_espresso'); ?></h3>
							
			<?php
							//Outputs the custom form questions. This function can be overridden using the custom files addon
							echo event_espresso_add_question_groups( $question_groups, '', NULL, FALSE, array( 'attendee_number' => 1 ), 'ee-reg-page-questions' );
			?>
						</div>
						
			<?php					
							//Coupons
			?>
						<input type="hidden" name="use_coupon[<?php echo $event_id; ?>]" value="<?php echo $use_coupon_code; ?>" />
			<?php
							if ( $use_coupon_code == 'Y' && function_exists( 'event_espresso_coupon_registration_page' )) {
								echo event_espresso_coupon_registration_page($use_coupon_code, $event_id);
							}
							//End coupons display
									
							//Groupons
			?>
						<input type="hidden" name="use_groupon[<?php echo $event_id; ?>]" value="<?php echo $use_groupon_code; ?>" />
			<?php
							if ( $use_groupon_code == 'Y' && function_exists( 'event_espresso_groupon_registration_page' )) {
								echo event_espresso_groupon_registration_page($use_groupon_code, $event_id);
							}
							//End groupons display					
			?>
						<input type="hidden" name="regevent_action" id="regevent_action-<?php echo $event_id; ?>" value="post_attendee">
						<input type="hidden" name="event_id" id="event_id-<?php echo $event_id; ?>" value="<?php echo $event_id; ?>">
						
			<?php
							//Multiple Attendees
							if ( $allow_multiple == "Y" && $number_available_spaces > 1 ) {					
								//This returns the additional attendee form fields. Can be overridden in the custom files addon.
								echo event_espresso_additional_attendees($event_id, $additional_limit, $number_available_spaces, __('Number of Tickets', 'event_espresso'), true, $event_meta);
							} else {				
			?>
						<input type="hidden" name="num_people" id="num_people-<?php echo $event_id; ?>" value="1">
			<?php
							}					
							//End allow multiple	
							
							wp_nonce_field('reg_nonce', 'reg_form_nonce');
							
							//Recaptcha portion
							if ( $org_options['use_captcha'] == 'Y' && empty($_REQUEST['edit_details']) && ! is_user_logged_in()) {
							
								if ( ! function_exists('recaptcha_get_html')) {
									require_once(EVENT_ESPRESSO_PLUGINFULLPATH . 'includes/recaptchalib.php');
								}
								# the response from reCAPTCHA
								$resp = null;
								# the error code from reCAPTCHA, if any
								$error = null;
			?>
						<p class="event_form_field" id="captcha-<?php echo $event_id; ?>">
							<?php _e('Anti-Spam Measure: Please enter the following phrase', 'event_espresso'); ?>
							<?php echo recaptcha_get_html($org_options['recaptcha_publickey'], $error, is_ssl() ? true : false); ?>
						</p>
							
			<?php 
							} 
							//End use captcha  
			?>
						<p class="event_form_submit" id="event_form_submit-<?php echo $event_id; ?>">
							<button id="event_form_field-<?php echo $event_id; ?>" type="submit" name="Submit" value="<?php _e('Submit', 'event_espresso'); ?>"> Submit    <i class="fa fa-chevron-right">    </i></button>
						</p>
						
			<?php 
						do_action('action_hook_espresso_registration_form_bottom', $event_id, $event_meta, $all_meta);
					}
			 ?>
					
			    </form>
			</div>
	
<?php 
				do_action('action_hook_espresso_registration_page_bottom', $event_id, $event_meta, $all_meta);
				break;
				
			}
			//End Switch statement to check the status of the event

		if (isset($ee_style['event_espresso_form_wrapper_close'])) {
			echo $ee_style['event_espresso_form_wrapper_close']; 
		}			
?>
	</div><!--col-xs-6-->
	<div id="venue-map" class="col-xs-6">
		<h1 class="title">Venue Map</h1>
		<iframe width="100%" height="600" frameborder="0" scrolling="no" marginheight="0" marginwidth="0" src="http://maps.google.com/?q=<?php echo $venue_address ?> <?php echo $venue_city ?> <?php echo $venue_state ?> <?php echo $venue_zip ?>&output=embed"></iframe>	
	</div>
</div><!--row-->

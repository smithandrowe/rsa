<?php
/**
 * The Header for our theme.
 *
 * Displays all of the <head> section and everything up till <div id="main">
 *
 * @package WordPress
 * @subpackage Smithandrowe
 * @since Smith and Rowe 1.0
 */
?><!DOCTYPE html>
<!--[if IE]>
<html class="ie">
<![endif]-->
<!--[if IE]>
<html>
<!--<![endif]-->
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0">
	<title><?php wp_title( '|', true, 'right' ); ?></title>
	<link rel="profile" href="http://gmpg.org/xfn/11">
	<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">
	<link rel="stylesheet" type="text/css" href="//cloud.typography.com/6605452/732502/css/fonts.css" />
	<link rel="stylesheet" type="text/css" href="<?php echo get_stylesheet_uri(); ?>" />
	<!--[if lt IE 9]>
		<script src="<?php echo get_template_directory_uri(); ?>/js/html5.js"></script>
	<![endif]-->
	<?php wp_head(); ?>
	<script type="text/javascript">
		var __lc = {};
		__lc.license = 3555701;
		
		(function() {
			var lc = document.createElement('script'); lc.type = 'text/javascript'; lc.async = true;
			lc.src = ('https:' == document.location.protocol ? 'https://' : 'http://') + 'cdn.livechatinc.com/tracking.js';
			var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(lc, s);
		})();
	</script>
	<script>
	  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
	  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
	  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
	  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

	  ga('create', 'UA-48130780-1', 'rsabarschoolmelbourne.com.au');
	  ga('send', 'pageview');

	</script>
    <?php if (is_page('9')) : ?>
        <!-- Google Code for Conversion Conversion Page -->
        <script type="text/javascript">
            /* <![CDATA[ */
            var google_conversion_id = 971844587;
            var google_conversion_language = "en";
            var google_conversion_format = "2";
            var google_conversion_color = "ffffff";
            var google_conversion_label = "mxZ1CP3axAgQ69e0zwM";
            var google_conversion_value = 39;
            var google_remarketing_only = false;
            /* ]]> */
        </script>
        <script type="text/javascript" src="//www.googleadservices.com/pagead/conversion.js">
        </script>
        <noscript>
            <div style="display:inline;">
                <img height="1" width="1" style="border-style:none;" alt="" src="//www.googleadservices.com/pagead/conversion/971844587/?value=39&amp;label=mxZ1CP3axAgQ69e0zwM&amp;guid=ON&amp;script=0"/>
            </div>
        </noscript>
    <?php endif; ?>


</head>

<body <?php body_class(); ?>>
<header>
	<div class="top">
		<div class="container">
			<div class="row">
				<div class="col-xs-2">
					<div class="top-left">
						<a href="<?php echo bloginfo('url'); ?>" alt="National Skills Group"><img src="<?php echo bloginfo('url'); ?>/images/gas-logo-colour.jpg" /></a>	
					</div>
				</div>
				<div class="col-xs-10">
					<div class="top-right">
						<p><?php echo eto_get_option('eto_topcontactinfo'); ?>
							
							<span> 
							
								<?php 
									
									$link = eto_get_option('eto_topcontactnumber'); 
									$email = '@';
									
									if (strpos($link, $email)) {
										echo '<a href="mailto:'. $link .'">'.$link .'</a>';
									}else {
										echo $link;
									}
								
								?>
							
							
							</span>
							
							
							</p>
					</div>
				</div>
			</div>
		</div>
	</div>
	<?php get_template_part('blocks/slider'); ?>
</header>

<?php
/**
 * The template for displaying 404 pages (Not Found).
 *
 * @package WordPress
 * @subpackage Smithandrowe
 * @since Smith and Rowe 1.0
 */

get_header(); ?>
<div class="container">
    <div class="row">
        <div class="col-xs-12">
            <h1>Sorry, page not found.</h1>
            <p>Click <a href="<?php echo bloginfo('url'); ?>">here</a> to go back.</p>
            <br />
            <br />
        </div>
    </div>
</div>
<?php get_footer(); ?>
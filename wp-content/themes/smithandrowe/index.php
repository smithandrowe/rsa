<?php
/**
 * The main template file.
 *
 * This is the most generic template file in a WordPress theme and one of the
 * two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * For example, it puts together the home page when no home.php file exists.
 *
 * Learn more: http://codex.wordpress.org/Template_Hierarchy
 *
 * @package WordPress
 * @subpackage Smithandrowe
 * @since Smith and Rowe 1.0
 */

get_header(); ?>
<div id="home" class="container">
	<div class="row">
		<div class="col-xs-12 col-sm-12 col-md-6 col-lg-5">			
			<?php get_template_part('blocks/accordion'); ?>
		</div>
		<div class="col-xs-12 col-sm-12 col-md-6 col-lg-7">
			<?php get_template_part('blocks/courses'); 
			//get_sidebar();
			?>
		</div>
	</div>
	<div class="row">
		<div class="col-xs-12 col-sm-12 col-md-9 col-lg-9">
			<?php get_template_part('blocks/testimonials'); ?>
		</div>
		<div class="col-xs-12 col-sm-12 col-md-3 col-lg-3">
			<?php get_template_part('blocks/more-courses'); ?>
		</div>
	</div>
</div>
<?php get_footer(); ?>
<div class="slider">

				<div class="main-slider">
					<ul class="slides">
						<li>
							<div class="container">
								<div class="row">
									<div class="col-xs-6">
										<h1>Get your RSA</h1>
										<p>From the very best trainers in the business.</p>
									</div>
									<div class="col-xs-6">
										<img src="<?php echo bloginfo('template_url'); ?>/images/slide-01.png" />
									</div>
								</div>
							</div>
						</li>
						<li>
							<div class="container">
								<div class="row">
									<div class="col-xs-6">
										<h1>Get trained and get work</h1>
										<p>In the best bars and restaurants in Melbourne.</p>
									</div>
									<div class="col-xs-6">
										<img style="margin-right: 20px" src="<?php echo bloginfo('template_url'); ?>/images/slide-02.png" />
									</div>
								</div>
							</div>
						</li>
						<li>
							<div class="container">
								<div class="row">
									<div class="col-xs-6">
										<h1>Have fun while learning</h1>
										<p>Be trained by the industry's finest.</p>
									</div>
									<div class="col-xs-6">
										<img style="margin-right: 20px" src="<?php echo bloginfo('template_url'); ?>/images/slide-03.png" />
									</div>
								</div>
							</div>
						</li>
					</ul>
				</div>
			</div>
		</div>
	</div>
</div>
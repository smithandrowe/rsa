<div class="testimonials">
	<div class="row">
		<div class="col-xs-12">
			<h1 class="title">Testimonials</h1>
		</div>
		<div class="col-xs-12 col-sm-4 col-md-4 col-lg-4">
			<p>"A couple that I served throughout the night stopped me on their way out and told me of how impressed they were with my service. I just want to thank you for teaching me everything you did over the hospitality course"</p>
		</div>
		<div class="col-xs-12 col-sm-4">
			<p>"Richard exceeded his role as a trainer and constantly went out of his way to provide material and interesting information. Thank you, Nanette"</p>
		</div>
		<div class="col-xs-12 col-sm-4">
			<p>"Thank you so much :) and even more to the fact you stayed back also for me… Your course will definitely help me achieve my future goals. Jamie"</p>
		</div>
	</div>
</div>

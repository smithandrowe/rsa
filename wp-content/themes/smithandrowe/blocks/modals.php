<div class="modal fade" id="contact">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h4 class="modal-title">Contact</h4>
      </div>
		<div class="modal-body">
			<div class="row">
			<div class="col-xs-6">
				<p><b>Goulburn Administration</b><br/><b>Services Limited</b><br />trading as GAS Training and Development<br />a division of National Skills Group</p>
			 	<p><b>ABN:</b><br />11 095 943 543</p>
			</div>
			<div class="col-xs-6">
				<p><b>Address:</b><br />144 George Street,<br />Fitzroy VIC 3065</p>
				<p><b>Phone:</b><br />(03) 9417 2410</p>
				<p><b>Fax:</b><br />(03) 9416 2386</p>
				<p><b>Email:</b><br /><a href="mailto:chris.smith@kbstraining.com.au">chris.smith@kbstraining.com.au</a></p>
			</div>
			</div>
		</div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->

<div class="modal fade" id="copyright">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h4 class="modal-title">Copyright</h4>
      </div>
      <div class="modal-body">
        <p>Unless otherwise expressly stated in the material, the following statements apply to all information on the National Skills Group website.</p>
        <p>Copyright © 2013, National Skills Group - All rights reserved.</p>
        <h4>Copyright Notice</h4>
		<p>This publication is copyright. Except as permitted by the Copyright Act, no part of it may in any form or by any means be reproduced or transmitted without the prior permission of the publisher. Commonwealth of Australia Copyright Regulations 1969.</p>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->

<div class="modal fade" id="privacy">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h4 class="modal-title">Privacy</h4>
      </div>
      <div class="modal-body">
        <div class="row">
        	<div class="col-xs-6">
        		<h4>Privacy</h4>
        		<p>National Skills Group aims to comply with the Information Privacy Act 2000 as well as the Commonwealth Privacy Act 1988 and to implement practices and procedures to ensure compliance. At National Skills Group we respect the privacy rights of individuals (our staff, clients and students) to security, privacy and service in regard to the personal information they supply to us. We will observe the following principles:</p>
				<h4>Collection of information</h4>
				<p>We are required to collect information, which is also stored on the Victorian Student Register.</p>
				<p>We will only collect information that is relevant. Information that is collected will be held securely to prevent any security breaches. Information that is collected will be processed in order to meet the individual needs of our staff, clients and students.</p>
				<h4>Use and Disclosure</h4>
				<p>We will not divulge any personal or health information to a third party for any reason other than the primary purpose for its collection or for purposes specified in our privacy notice or with the consent of the individual or as required by law. At National Skills Group we value our staff, clients and students and will respect the privacy of their personal information. The Victoria Government or ASQA may request access to information including student files for audit or research information purposes.  Our company is obligated to provide this information to the government on request.
				<h4>Data Quality</h4>
				<p>We will take all reasonable steps to ensure the information that is collected is complete, accurate and current.</p>
				<p>If a staff member, client or student wishes to access or up date their personal or health information we will provide all reasonable assistance with this.</p>
				<h4>Data Security</h4>
				<p>We will take all reasonable steps to ensure that information is protected from misuse, unauthorised access, modification or disclosure. All information not required will be destroyed in accordance with privacy legislation or as required by other legislation or as required under guidance from the Public Records Office.</p>
				<h4>Openness</h4>
				<p>We will take all reasonable steps to provide our staff, clients and students with details of their personal information being held upon request. We will advise them of the type of information we possess, the purpose for it being held, the method of collection, use and disclosure of the information as well as their rights to access and amend this information.</p>
        	</div>
        	<div class="col-xs-6">
        		<h4>Access and Correction</h4>
				<p>In most circumstances we will give staff, clients and students access to their personal or health information upon request. All requested information will be provided within 14 days from receipt of the request. All information that is not accurate will be amended within 5 days of receiving a written request to do so.</p>
				<p>We seek to have accurate records so information needed to update these records such as current contact details will be made on request.</p> 
				<h4>Unique identifiers</h4>
				<p>Sometimes we have to collect unique identifiers such as Centrelink numbers, Tax File Numbers or Health Care Card Numbers. If we do need this information the purposes for collecting these numbers will be explained to individuals.</p>
				<p>We will not use these unique identifiers for any other purposes than those for which they were collected.</p>
				<p>If we ascribe a unique identifier to an individual for internal use this will not be shared with any other body or person without the consent of the individual.</p>
				<h4>Transborder data flows</h4>
				<p>We will not transfer personal information to a person outside of Victoria or Australia unless that person or body is legally obliged to protect the individual’s privacy under equivalent or higher privacy legislation as ourselves. In most circumstances the transborder transfer of personal or health information that we hold about an individual will only be transferred with that individual’s consent.</p>
				<h4>Sensitive Information</h4>
				<p>We will only collect sensitive information about an individual with consent of the person or if required by law. “Sensitive information” - information or an opinion about an individual’s:</p>
				<ul>
					<li>Racial or ethnic origin</li>
					<li>Political opinions</li>
					<li>Membership of a political association</li>
					<li>Religious belief or affiliations</li>
					<li>Sexual preferences</li>
					<li>Criminal record</li>
					<li>Membership of a trade union</li>
					<li>Membership of a professional or trade association</li>
				</ul>
				<p>National Skills Group will only contract with and supply personal information with suppliers, contractors and employers who agree to adhere to our privacy policy and all relevant privacy legislation.</p>
				<p>If you would like more information regarding our Privacy policy, contact Karen Bailey on mobile 0408 600 335 or email at Karen@kbstraining.com.au</p>		
        	</div>
        </div>
        		
		
		
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->
<div class="modal fade" id="refunds">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h4 class="modal-title">Refunds</h4>
      </div>
      <div class="modal-body">
        <div class="row">
        	<div class="col-xs-12">
        		<p>A refund will only be considered if requested in writing.<br />
				Once the RSA session has been completed, a refund will not be 
				considered.<br />
				If you cannot attend the session you have enrolled in and we cannot 
				schedule you into another course, we will refund monies paid to you.</p>
        	</div>
        </div>
        		
		
		
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->
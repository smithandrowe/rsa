<div class="courses">
	<div class="info">
		<h1>Upcoming RSA Courses</h1>
		<div class="ribbon">
			<span>Only</span>
			<h2>$39</h2>
			<small>.00</small>
		</div>
        <div class="location">
            <p class="_label"> Course Location: <span class="_place">Fitzroy Town Hall Hotel</span> </p>

            <ul class="buttonSet">
                <li>
                    <a target="_blank"target="_blank" href="https://www.google.com/maps/place/166+Johnston+St/@-37.798072,144.98041,18z/data=!4m2!3m1!1s0x6ad6432060757c6d:0x10c17e4527acd76a?hl=en-US">
                        <img src="/wp-content/themes/smithandrowe/images/pin-map-icon.png" /> Click for map
                    </a>
                </li>
                <li><i class="fa fa-clock-o"></i> Duration: 4 hours</li>
            </ul>
	    </div>

    </div>
	<div class="table-responsive">
		<table class="table table-striped table-hover">
        <thead>
          <tr>
            <th style="width:180px">Date</th>
            <th style="width:136px">Start Time</th>
            <th style="width:102px">Price</th>
            <th>Available  seats</th>
          </tr>
        </thead>
        <tbody>
        	<?php echo do_shortcode('[EVENT_LIST limit="37"]'); ?>
        </tbody>
      </table>
	</div>
	
     
</div>

<div class="more-courses">
	<div class="row">
		<div class="col-xs-12">
			<h1 class="title">More Courses</h1>
			<p>We are currently developing a series of courses including a hospitality skills job-ready program that will provide you with the skills and improved confidence to find a job in hospitality.
An outline of this program will be coming soon.</p>
		</div>
	</div>
</div>

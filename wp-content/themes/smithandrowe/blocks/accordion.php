<div class="panel-group" id="accordion">
  <div class="panel panel-default">
    <div class="panel-heading">
		<a data-toggle="collapse" data-parent="#accordion" href="#collapseOne">
      		<h4 class="panel-title">Benefits <i class="fa fa-angle-up"> </i></h4>
      	</a>
    </div>
    <div id="collapseOne" class="panel-collapse collapse in">
      <div class="panel-body">
        <p>Start work in the hospitality industry now by gaining your RSA certification.</p>
        <p>GAS Training and Development offers the very best in training, being the training partner of some of Australia's leading Hotels and Restaurants.</p>
        <p>Our trainers have extensive experience in Australian and international hospitality venues and have worked at award-winning venues including: "The Pier" Rose Bay, "Icebergs Dining Room & Bar" Bondi Beach, "The Stokehouse" St Kilda, "The Savoy" and "The Ritz" London.</p>
        <p>Have fun while being trained by some of the industry’s finest and get working in the best bars and restaurants in town.</p>
 		<ul>
 			<li>Your certificate will be issued within 4 days.</li>
 			<li>Certification by the Victorian Commission for Gambling and Liquor Regulation (VCGLR).</li>
 			<li>Inner city location</li>
 		</ul>
      </div>
    </div>
  </div>
  <div class="panel panel-default">
    <div class="panel-heading">
		<a data-toggle="collapse" data-parent="#accordion" href="#collapseTwo">
      		<h4 class="panel-title">Course Outline <i class="fa fa-angle-down"> </i></h4>
      	</a>
    </div>
    <div id="collapseTwo" class="panel-collapse collapse">
      <div class="panel-body">
        <h1>RSA - Responsible Service of Alcohol</h1>
        <p>Undertaking an approved Responsible Service of Alcohol (RSA) program gives participants the skills and knowledge necessary to contribute to a safe, enjoyable environment in licensed premises.</p>
        <p>Face-to-face RSA training is mandatory for licensees and staff selling, offering or serving liquor for general, on-premises, late night and packaged liquor licences.</p>
        <p>Licensees and staff subject to mandatory RSA requirements have one month from the date on which they first sell, offer for sale or serve liquor on the licensed premises to complete an RSA program.</p>
        <p>The RSA program covers a range of topics including:</p>
	 	<ul>
	 		<li>Problems associated with excessive consumption</li>
	 		<li>Alcohol and the law</li>
	 		<li>The question of who is responsible for RSA</li>
	 		<li>Facts about alcohol</li>
	 		<li>Improving the atmosphere of your premises</li>
			<li>Handling difficult customers</li>
	 	</ul>
      </div>
    </div>
  </div>
  <div class="panel panel-default">
    <div class="panel-heading">
      <a data-toggle="collapse" data-parent="#accordion" href="#collapseThree">
      		<h4 class="panel-title">FAQs <i class="fa fa-angle-down"> </i></h4>
      	</a>
    </div>
    <div id="collapseThree" class="panel-collapse collapse">
      <div class="panel-body">
        <ol>
        	<li>Why do I need to attend a class, can’t I complete this course online?</li>
        	<p>In Victoria, you must attend a class based course delivered by a certified trainer in order to gain your RSA certificate.</p>
        	<li>I completed an RSA course in Queensland, can I supply this to my employer?</li>
        	<p>Nationally recognised RSA online training is not accepted in Victoria nor are certificates from other States. People who have attained RSA Certificates from interstate face to face can apply for mutual recognition if their RSA training meets the criteria for mutual recognition. </p>
        	<li>When will I receive my certificate?</li>
        	<p>Within 4 days following successful completion of the course.</p>
        	<li>How long will my certificate be valid for?</li>
        	<p>The RSA certificate will need to be refreshed every three years to be valid.  Students are required to keep their original RSA certificate and attach the RSA updates to the Certificate to prove that they are current. Refresher courses are available online at the Victorian Commission for Gambling and Liquor Regulation website. Students <b>must</b> attend a face to face course to initially obtain their RSA.</p>
        	<li>What do I bring on the day?</li>
        	<ul>
        		<li>A form of photo ID with your name and current address on it</li>
        		<li>Yourself and a positive attitude</li>
        	</ul>
        	<li>Do I need to be 18 years of age to complete the training?</li>
        	<p>You have to be 18 years of age to work with liquor in the hospitality industry. </p>
        </ol>
      </div>
    </div>
  </div>
  <div class="panel panel-default">
    <div class="panel-heading">
      <a data-toggle="collapse" data-parent="#accordion" href="#collapseFour">
      		<h4 class="panel-title">Our Trainers <i class="fa fa-angle-down"> </i></h4>
      	</a>
    </div>
    <div id="collapseFour" class="panel-collapse collapse">
      <div class="panel-body">
        <h1>Rebecca Green</h1>
        <p>Rebecca has been in the hospitality industry since she can remember. She has served Sydney’s elite whilst working at many prestigious waterfront venues, she rates looking after our team of athletes during the Sydney Olympics as a highlight. For 5 years she was the Manager at The Stokehouse Cafe in St. Kilda.  This iconic split level beach front restaurant has been an institution for the last 22 years.  Seating up to 250 guests, she understands pressure and time management as her team served up to 1000 guests a day. Rebecca is equally at home spoiling a small table of International guests as she is with organising an unforgettable New Year’s Eve event. Nothing is too small or big a challenge for Rebecca.</p>
        <h1>Megan Benett</h1>
        <p>Megan originally started out in hospitality in Perth, and moved to Melbourne to work in the hospitality capital of Australia. She has worked extensively in bars, restaurants and event management over the years. Most recently she managed True South Restaurant and Brewery in Black Rock which comprises of an onsite brewery, 100 seat restaurant and an event space overlooking the bay.  She is very happy behind a cocktail shaker or arranging glorious vases of flowers whilst putting together detailed running sheets for a beachfront event.</p>
      </div>
    </div>
  </div>
</div>
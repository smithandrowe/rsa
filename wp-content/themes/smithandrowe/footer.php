<?php
/**
 * The template for displaying the footer.
 *
 * Contains footer content and the closing of the
 * #main and #page div elements.
 *
 * @package WordPress
 * @subpackage Smithandrowe
 * @since Smith and Rowe 1.0
 */
?>
<footer>
	<div class="container">
		<div class="row">
			<div class="col-xs-12 col-sm-6">
				
				<ul class="footer-left">
					<li data-toggle="modal" data-target="#copyright">&copy; Copyright 2013</li>
					<li>GAS Training and Development</li>
					<li>TOID 3836</li>
					<li class="last">ABN 11 095 943 543</li>
				</ul>
			</div>
			<div class="col-xs-12 col-sm-6">
				<ul id="footer-controls">
					<li data-toggle="modal" data-target="#contact">Contact</li>
					<li data-toggle="modal" data-target="#privacy">Privacy Policy</li>
					<li data-toggle="modal" data-target="#refunds">Refunds</li>
					<li class="last"><a href="http://www.smithandrowe.com.au" title="Smith and Rowe" target="_blank">Dev by S&R</a></li>
				</ul>
				
			</div>
		</div>
	</div>
</footer>
<?php get_template_part('blocks/modals'); ?>
	<?php wp_footer(); ?>
</body>
</html>
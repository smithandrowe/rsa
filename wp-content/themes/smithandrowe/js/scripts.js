;(function($) {
    'use strict';
	$(document).ready(function() {
		$('.main-slider').flexslider({
			animation: "slide",
			directionNav: true,
			controlNav: false,
			before: function(slider){
				
				if (slider.animating) {
					
					if($('.slider').hasClass('more-bg')) {
						$('.slider').addClass('less-bg');
						$('.slider').removeClass('more-bg');
					}else {
						$('.slider').addClass('more-bg');
					}
				}	
			}
		});
		$('.panel-heading a').each(function() {
			
			$(this).on('click', function(){
				/*
				if ($(this).parent().find('.panel-collapse').hasClass('in')){
					$(this).find('.panel-title i').removeClass('fa-angle-down').addClass('fa-angle-up');
				}else{
					$(this).find('.panel-title i').removeClass('fa-angle-up').addClass('fa-angle-down');
				}*/
				if ($(this).parent().parent().find('.panel-collapse').hasClass('in')) {
					$(this).parent().parent().find('.panel-title i').removeClass('fa-angle-up').addClass('fa-angle-down');
				}else {
					$(this).parent().parent().find('.panel-title i').removeClass('fa-angle-down').addClass('fa-angle-up');
				}
			});
		});
		
		$(".event-form-submit-btn").on('click', function(){
			$('#phone').addClass('error');
		});
		$('#collapseOne').on('hidden.bs.collapse', function () {
			  $(this).parent().find('.panel-title i').removeClass('fa-angle-up').addClass('fa-angle-down');
  		});
  		$('#collapseTwo').on('hidden.bs.collapse', function () {
			  $(this).parent().find('.panel-title i').removeClass('fa-angle-up').addClass('fa-angle-down');
  		});
  		$('#collapseThree').on('hidden.bs.collapse', function () {
			 $(this).parent().find('.panel-title i').removeClass('fa-angle-up').addClass('fa-angle-down');
  		});
  		$('#collapseFour').on('hidden.bs.collapse', function () {
			  $(this).parent().find('.panel-title i').removeClass('fa-angle-up').addClass('fa-angle-down');
  		});
  		$('#collapseOne').on('show.bs.collapse', function () {
			  $(this).parent().find('.panel-title i').removeClass('fa-angle-down').addClass('fa-angle-up');
  		});
  		$('#collapseTwo').on('show.bs.collapse', function () {
			  $(this).parent().find('.panel-title i').removeClass('fa-angle-down').addClass('fa-angle-up');
  		});
  		$('#collapseThree').on('show.bs.collapse', function () {
			 $(this).parent().find('.panel-title i').removeClass('fa-angle-down').addClass('fa-angle-up');
  		});
  		$('#collapseFour').on('show.bs.collapse', function () {
			  $(this).parent().find('.panel-title i').removeClass('fa-angle-down').addClass('fa-angle-up');
  		});
  		
	}); //end document ready
})(jQuery);

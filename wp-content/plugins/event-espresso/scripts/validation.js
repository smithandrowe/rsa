$jaer = jQuery.noConflict();
	jQuery(document).ready(function($jaer) {
	jQuery(function(){
		//Registration form validation
		jQuery.validator.addMethod("phoneAU", function(value, element) {
			return this.optional(element) || /^(\+?61|0)\d{9}$/.test(value.replace(/\s+/g, ""));
		}, "Please specify a valid phone number");
		jQuery('#registration_form').validate({rules: {
            phone: {
                required: true,
                phoneAU: true
            }
        }});
	});
});
